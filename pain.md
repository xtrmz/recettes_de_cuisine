## Recette du pain

Avec ces quantités, vous pourrez faire quatre gros pains pour environ une trentaine de personnes.

### Ingrédients

* 2,5 kilos de farine
* 20 grammes de levure fraiche de boulanger
* 30 grammes de sel
* 1,5 L d'eau

### Préparation 

La pate se prépare en deux étapes.

#### Préparation de la pâte
* Bien se savonner les mains
* Mélanger farine, sel avec les deux mains dans un grand récipient
* Dissoudre la levure avec un peu d'eau tiède
* Ajouter la levure et son eau dans le grand récipient 
* Ajouter de l'eau au fur et à mesure et pétrir de 5 à 7 minutes avec **une seule main** (afin de garder une main propre)
* Laisser lever la pâte pendant 2 heures dans le grand récipient recouvert d'un torchon humide

#### Préparation du pain
* Mettre le pain en forme de boule, ajouter un peu de farine
* Faire attention de ne pas trop malaxer le pain
* Laisser lever pendant 10 minutes

### Cuisson
* Faire préchauffer le four à 240
* Avant d'enfourner, tracer plusieurs lignes au couteau sur le dessus du pain pour permettre de lever malgré la croûte pendant la cuisson
* Enfourner le pain sur du papier sulfurisé ou sur une plaque
* Verser 3/4 d'un verre d'eau dans le four
* Laisser cuire 45 minutes


